package tree_sitter_eno_test

import (
	"testing"

	tree_sitter "github.com/smacker/go-tree-sitter"
	"github.com/tree-sitter/tree-sitter-eno"
)

func TestCanLoadGrammar(t *testing.T) {
	language := tree_sitter.NewLanguage(tree_sitter_eno.Language())
	if language == nil {
		t.Errorf("Error loading Eno grammar")
	}
}
